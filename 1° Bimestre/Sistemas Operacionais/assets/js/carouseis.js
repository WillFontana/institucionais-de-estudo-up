// -- chamada dos carroseis -- //
// -- chamada dos carroseis -- //
function callCarouseis() {
  // carousel banner
  $('#carousel-banner').owlCarousel({
    loop: true,
    dots: false,
    nav: false,
    items: 1,
    autoplay: true,
    autoplayHoverPause: true,
    mouseDrag: true,
  })
  BlogCarousel.owlCarousel({
    loop: true,
    dots: false,
    nav: false,
    items: 3,
    autoplay: true,
    autoplayHoverPause: true,
    mouseDrag: true,
    responsive: {
      // breakpoint telas inferiores a 768px
      0: {
        items: 2,
      },
      //breakpoint telas inferiores a 1024px
      768: {
        items: 3,
      },
      //breakpoint telas superiores a 1024
      1024: {
        items: 3,
      }
    }
  })


  // Carousel de produtos
  ProductCarousel.owlCarousel({
    loop: true,
    dots: false,
    nav: false,
    items: 4,
    autoplay: true,
    autoplayHoverPause: true,
    mouseDrag: true,
    responsive: {
      // breakpoint telas inferiores a 768px
      0: {
        items: 2,
      },
      //breakpoint telas inferiores a 1024px
      768: {
        items: 3,
      },
      //breakpoint telas superiores a 1024
      1024: {
        items: 4,
      }
    }
  })
  
  // Carousel da empresa
  EmpresaCarousel.owlCarousel({
    loop: false,
    dots: false,
    nav: false,
    items: 4,
    autoplay: true,
    autoplayHoverPause: true,
    mouseDrag: true,
    responsive: {
      // breakpoint telas inferiores a 768px
      0: {
        items: 2,
      },
      //breakpoint telas inferiores a 1024px
      768: {
        items: 3,
      },
      //breakpoint telas superiores a 1024
      1024: {
        items: 4,
      }
    }
  })
}
