// -- document onload -- //
$(window).on('load', function () {
    // procura os insputs preenchidos para adicionar a classe used
    apllyUsed();
    // aplica o dotdotdot
    $('.dotdotdot').dotdotdot();
    // Adapta a altura do body a altura do header
    $('#html-body').css('padding-top', $('#main-header').innerHeight() + 'px');
    // suaviza o redirecionamento do menu
    $(document).on('click', 'a[href^="#"]', function (event) {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top - 68
        }, 2000);
    });
    // abre e fecha o menu 
    $('#main-burger').on('click', function () {
        toggleMenu();
    })
    // abre e fecha o fancybox
    $('a.fancy-open').fancybox({
        'transitionIn': 'elastic',
        'transitionOut': 'elastic',
        'speedIn': 600,
        'speedOut': 200,
        'overlayShow': false
    });
    $(window).on('resize', function () {
        $('.dotdotdot').dotdotdot();
        $('#html-body').css('padding-top', $('#main-header').innerHeight() + 'px')
    })
    // navegação do menu-mobile
    $('.list-item').on('click', function () {
        $('.list-item').removeClass('-active');
        $(this).addClass('-active');
        toggleMenu();
    })
    // fixa o menu caso ele esteja a uma certa altura
    scrollHeader();
    $(window).scroll(scrollHeader);
    // fixa o span ao digitar no teclato
    $('.input').on('input', function () {
        if ($(this).val()) {
            $(this).addClass('-used');
        } else {
            $(this).removeClass('-used');
        }
    });
});

