// --- funções --- //

// Abre e fecha o menu
function toggleMenu() {
  $('body').toggleClass('-mobile-overlayed');
  $('#main-burger').toggleClass('-active');
  $('#main-nav').toggleClass('-show');
  $('#desklogo').toggleClass('-hide');
}

// função que seta os dots nas caixas respectivas
function apllyDotDot() {
  $('html').find('.dotdot').each(function () {
    var dotBoxHeight = $(this).height();
    var dotChildHeight = $(this).children().innerHeight();
    if (dotBoxHeight <= dotChildHeight) {
      $(this).addClass('-doted');
    }
  });
};
// procura os insputs preenchidos para adicionar a classe used
function apllyUsed() {
  $('html').find('.input').each(function () {
    if ($(this).val()) {
      $(this).addClass('-used');
    } else {
      $(this).removeClass('-used');
    }
  });
};
// função para fechar a warningbox
// ** chama-la sempre dentro de um setTimeout de 6 segundos ** 
function fadeWarning() {
  $('.warning-box').addClass('-fading');
  setTimeout(function () {
    $('.warning-box').addClass('-gone');
    $('.warning-box').removeClass('-fading');
  }, 200);
};

// função de resize do textarea
// Adicionar ao textarea onkeydown="autosize('id')"
function autosize(id) {
  var el = document.getElementById(id);
  setTimeout(function () {
    el.style.cssText = 'height:auto; padding:0';
    el.style.cssText = 'height:' + el.scrollHeight + 'px';
  }, 0);
}

// função que fixa o menu no topo
function scrollHeader() {
  if ($(window).scrollTop() > $('.header').height()) {
    $('.header').addClass('-fixed');
  } else {
    $('.header').removeClass('-fixed');
  }
}

// função que remove o player do video
function playVideoA() {
  $('#videoA').addClass('-open-video');
}

function playVideoB() {
  $('#videoB').addClass('-open-video');
}

// Navegação do carousel de produtos
function prevProduct() {
  ProductCarousel.trigger('prev.owl.carousel');
}

function nextProduct() {
  ProductCarousel.trigger('next.owl.carousel');
}


// Navegação do carousel do blog
function prevBlog() {
  BlogCarousel.trigger('prev.owl.carousel');
}

function nextBlog() {
  BlogCarousel.trigger('next.owl.carousel');
}

// Navegação do carousel das categorias
function prevCategory() {
  CategoryCarousel.trigger('prev.owl.carousel');
}

function nextCategory() {
  CategoryCarousel.trigger('next.owl.carousel');
}

// Navegação do carousel da empresa
function prevEmpresa() {
  EmpresaCarousel.trigger('prev.owl.carousel');
}

function nextEmpresa() {
  EmpresaCarousel.trigger('next.owl.carousel');
}


// teste de ativação de items na subcategoria
function activateSubcategoria(el) {
  var id = el;
  $('.sub-category-card').removeClass('-active');
  $('.subcategory-item').removeClass('-active');
  $('.category-item').removeClass('-active');
  $(el).addClass('-active');
}

function accordionCategory(ele) {
  var id = ele.id;
  var thisAccordion = $('#accordion-' + id);
  if (thisAccordion.hasClass('-open')) {
    $('.accordion-categoria').removeClass('-open');
    setTimeout(function () {
      $('.accordion-categoria').removeClass('-opening');
    }, 500)
  } else {
    $('.accordion-categoria').removeClass('-open');
    $('.accordion-categoria').removeClass('-opening');
    thisAccordion.addClass('-opening');
    setTimeout(function () {
      thisAccordion.addClass('-open');
    }, 500)
  }
}

function openCommonAccordion(idEl) {
  var id = idEl.id;
  var thisComonAccordion = $('#accordion-' + id);
  console.log(thisComonAccordion);
  if (thisComonAccordion.hasClass('-closed')) {
    thisComonAccordion.removeClass('-closed');
  } else {
    thisComonAccordion.addClass('-closed');
  }
}

// função para mudar a imagem da galeria
function changeMainMockup(product) {
  var current = $('.current-selected');
  var productId = product.id;
  var extraContent = $('#' + productId);
  $.each($('.current-selected'), function () {
    var OldId = this.id;
    // Adiciona a foto antiga para o item clicado
    extraContent.attr("id", OldId).attr('style', 'background-image: url(assets/img/product/product-' + OldId + '.jpg');
    // Modifica a foto antiga pela atual
    current.attr("id", productId).attr('style', 'background-image: url(assets/img/product/product-' + productId + '.jpg');
    $('#fancy-product').attr("href", 'assets/img/product/product-' + productId + '.jpg')
  });
}